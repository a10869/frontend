import { defineConfig } from 'vite';
import { resolve, dirname } from 'path';
import { fileURLToPath } from 'url';

import react from '@vitejs/plugin-react';

const __dirname = dirname(fileURLToPath(import.meta.url));
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  server: {
    port: 51200,
    cors: false,
    proxy: {
      '/store/public/': {
        target: '...',
        secure: false,
        changeOrigin: false
      }
    }
  },
  resolve: {
    alias: {
      '@': resolve(__dirname, 'src'),
      '#root': resolve(__dirname)
    }
  },
  build: {
    cssMinify: true,
    sourcemap: false,
    minify: true,
    commonjsOptions: {
      include: ['./packages/', /node_modules/]
    }
  }
});
