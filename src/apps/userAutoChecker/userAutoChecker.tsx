import { Col, Container, Row, Spinner } from 'react-bootstrap';
import { useState } from 'react';
import AutoCheckingSection from '@/packages/components/autoChecker/AutoCheckingSection.tsx';
import UserAutoCheckerLogs from '@/apps/userAutoChecker/userAutoCheckerLogs.tsx';
import {
  mergePipelinesWithJobs,
  useQueryGetUserPipelines
} from '@/apps/userAutoChecker/use-query-get-user-pipelines-rt.tsx';
import { useStoreAutoCheckerJobsUpdatedWs } from '@/packages/rq/use-store-pnet-jobs-ws.ts';

function UserAutoChecker() {
  const pipelineData = useQueryGetUserPipelines();
  const jobsWSData = useStoreAutoCheckerJobsUpdatedWs();
  const [jobLogId, setJobLogId] = useState<number | undefined>(undefined);
  const refreshFn = () => {
    pipelineData.refetch({ cancelRefetch: true });
  };
  return (
    <Container fluid>
      <Row>
        <Col sm={12} md={6}>
          {!pipelineData.isFetching && pipelineData.data ? (
            <AutoCheckingSection
              broomView={pipelineData.data?.is_admin}
              pipelines={mergePipelinesWithJobs(pipelineData.data, jobsWSData)}
              refreshFn={refreshFn}
              jobOnSelect={setJobLogId}
            />
          ) : null}
          {pipelineData.isFetching ? <Spinner size='sm' animation='border' variant='primary' /> : null}
        </Col>
        <Col sm={12} md={6}>
          <UserAutoCheckerLogs jobId={jobLogId} />
        </Col>
      </Row>
    </Container>
  );
}

export default UserAutoChecker;
