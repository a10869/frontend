import { Col, Container, Form, Row } from 'react-bootstrap';
import { useReloadContentJobLogs } from '@/apps/userAutoChecker/use-query-logs.tsx';

interface AutoCheckingLogsProps {
  jobId?: number;
}

function UserAutoCheckerLogs({ jobId }: AutoCheckingLogsProps) {
  const content = useReloadContentJobLogs(jobId ?? 0);
  if (!jobId) return null;
  return (
    <Container fluid>
      <Row className='justify-content-end p-2'>
        <Col xs={12}>
          <p className='mb-0 fw-bold'>Файл журнала</p>
        </Col>
      </Row>
      <Row className='p-2'>
        <Col>
          <Form.Control
            as='textarea'
            style={{
              height: 'calc(100vh - 60px)',
              backgroundColor: 'black',
              color: 'white'
            }}
            value={content ?? '...'}
            readOnly
          />
        </Col>
      </Row>
    </Container>
  );
}

export default UserAutoCheckerLogs;
