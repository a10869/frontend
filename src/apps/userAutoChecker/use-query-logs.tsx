import { useInfiniteQuery } from '@tanstack/react-query';
import { useEffect, useState } from 'react';
import { PnetInternalApi } from '@/packages/pnet-internal-api';

export function useQueryGetUserJobLogs(id: number) {
  return useInfiniteQuery(
    ['useQueryGetUserPipelines', id],
    ({ pageParam = 0 }) => {
      return PnetInternalApi.userAutoChecker.logs(id, pageParam);
    },
    {
      enabled: Boolean(id),
      staleTime: 30 * 1000,
      cacheTime: 5 * 60 * 1000,
      getNextPageParam: (result) => {
        return result.logs?.next;
      }
    }
  );
}

export function useReloadContentJobLogs(id: number) {
  const query = useQueryGetUserJobLogs(id);
  const [reload, setReload] = useState<boolean>(true);
  useEffect(() => {
    setReload(true);
  }, [id]);
  useEffect(() => {
    if (!reload) return () => {};
    const timerRef = setInterval(() => {
      if (!query.data?.pages) return;
      const lastPage = query.data.pages[query.data.pages.length - 1];
      if (!lastPage.job) return;
      if (lastPage.job.status === 'run') {
        query.fetchNextPage();
        setReload(true);
        return;
      }
      setReload(false);
    }, 3_000);
    return () => {
      clearInterval(timerRef);
    };
  }, [query, reload, id]);
  return query.data?.pages.map((page) => page.logs?.content || '')?.join('');
}
