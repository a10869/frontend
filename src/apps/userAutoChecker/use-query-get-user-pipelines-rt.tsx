import { useQuery } from '@tanstack/react-query';
import { PnetInternalApi } from '@/packages/pnet-internal-api';
import {
  IJobAutoChecker,
  IPipelineAutoChecker,
  IUserAutoCheckerGetResponse
} from '@/packages/pnet-internal-api/user-auto-checker-types.ts';
import { ClientWsMessage } from '@/packages/pnet-internal-ws/client-ws-types.ts';
import { MergeUtilsAbstract } from '@/packages/rq/mergeUtils.ts';

export interface IPipelineAutoCheckerWithJobs extends IPipelineAutoChecker {
  jobs: IJobAutoChecker[];
}

export function useQueryGetUserPipelines() {
  return useQuery(['useQueryGetUserPipelines'], async () => {
    return PnetInternalApi.userAutoChecker.get();
  });
}

class UseQueryGetUserPipelineJobItem extends MergeUtilsAbstract<IJobAutoChecker> {
  // eslint-disable-next-line class-methods-use-this
  public GrantEqual(first: IJobAutoChecker, second?: IJobAutoChecker): boolean {
    if (!second) return true;
    return first.updated_at >= second.updated_at;
  }

  // eslint-disable-next-line class-methods-use-this
  public Index(first: IJobAutoChecker): string {
    return `${first.id}`;
  }
}

export function mergePipelinesWithJobs(
  steps: IUserAutoCheckerGetResponse,
  jobsWs: ClientWsMessage<IJobAutoChecker>[]
): IPipelineAutoCheckerWithJobs[] {
  const mergeJobs = new UseQueryGetUserPipelineJobItem();
  mergeJobs.fromArray(steps.jobs);
  mergeJobs.fromArray(jobsWs.map((item) => item.extras.payload));
  const jobs = mergeJobs.toArray().sort((a, b) => b.id - a.id);
  const jobAndStepId = new Map<string, IJobAutoChecker[]>();

  jobs.forEach((job) => {
    const key = job.step_id.toString();
    const value = jobAndStepId.get(key) || [];
    value.push(job);
    jobAndStepId.set(key, value);
  });
  return steps.pipelines.map((item) => {
    return {
      jobs: jobAndStepId.get(item.id.toString()) || [],
      ...item
    };
  });
}
