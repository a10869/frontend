import { PnetInternalApi } from '@/packages/pnet-internal-api';
import { TAutoCheckingActions } from '@/packages/components/autoChecker/AutoCheckingActions';

export function actionsApiAutoChecker(refresh: () => void, id: string, action: TAutoCheckingActions) {
  const functions = {
    restart: PnetInternalApi.userAutoChecker.run.bind(PnetInternalApi.userAutoChecker),
    start: PnetInternalApi.userAutoChecker.run.bind(PnetInternalApi.userAutoChecker),
    cancel: PnetInternalApi.userAutoChecker.cancel.bind(PnetInternalApi.userAutoChecker)
  };
  functions[action](id).finally(refresh);
}

export function wipeApiAutoChecker(refresh: () => void) {
  // eslint-disable-next-line no-alert
  const isWipe = window.confirm('Будет совершена очистка среды тестирования лабораторной работы');
  if (!isWipe) {
    refresh();
    return;
  }
  PnetInternalApi.userAutoChecker.wipe().finally(refresh);
}
