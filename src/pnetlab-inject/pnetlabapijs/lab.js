document.addEventListener('DOMContentLoaded', function () {
  const autoCheckerUrl = '/store/public/user/autoChecker/view';
  const autoCheckerBtnLabs = `
        <div id="autochecker-btn-labs" class="box_flex button btn-xs btn btn-primary" style="margin: auto; display: flex;">
            <i class="icon fa fa-solid fa-check" style="font-size: 14px;"></i>
            &nbsp;CHECK&nbsp;
        </div>
    `;
  const autoCheckerModal = `
<div class='modal fade click ui-draggable active' id='autochecker-modal-labs' data-backdrop='false' data-keyboard='false'
     style='height: 0px; overflow: unset; top: 30px; left: 25%; display: none;'>
    <div class='modal-dialog modal-lg' role='document' style='margin: 0px; height: 0px;'>
        <div class='modal-content'
             style='min-height: 300px; flex-direction: column; display: flex; height: 500px; width: 650px;'>
            <div class='modal-header' style='height: 25px; min-height: unset; cursor: move;'><span
                    class='modal-title'>Check</span>
                <i id='autochecker-modal-labs-close' type='button' class='close' data-dismiss='modal'
                   aria-hidden='true'>×</i>
                <i title='Make transparent'
                   class='glyphicon glyphicon-certificate pull-right action-changeopacity'></i>
                   <i
                   id='autochecker-modal-labs-new-tab'
                   title='Open in new tab' class='fa fa-clone pull-right button'
                   style='font-size: 14px; color: rgb(166, 179, 185);'></i>
            </div>
            <div style='flex-direction: column; flex-grow: 1; display: flex;'>
                <div class='html_console' style='flex-grow: 1; display: flex;'>
                    <div style='width: 100%; display: block;'>
                        <iframe src='${autoCheckerUrl}' height='100%' width='100%'></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
`;

  const autoCheckerApi = {
    btnLoaded: false
  };
  // По клику на элемент загрузится новые элементы для лабораторной работы
  document.body.addEventListener(
    'click',
    function () {
      const loadingLabElement = document.getElementById('loading-lab');
      const wbBarElement = document.querySelector('.wb_bar.box_flex');
      if (autoCheckerApi.btnLoaded || wbBarElement == null || loadingLabElement == null) return;
      wbBarElement.innerHTML += autoCheckerBtnLabs;
      wbBarElement.innerHTML += autoCheckerModal;
      autoCheckerApi.btnLoaded = true;
      // Добавляем свойства для созданных элементов
      document.getElementById('autochecker-btn-labs').addEventListener('click', autoCheckerModalToggle, false);
      document.getElementById('autochecker-modal-labs-close').addEventListener('click', autoCheckerModalToggle, false);
      document
        .getElementById('autochecker-modal-labs-new-tab')
        .addEventListener('click', autoCheckerModalNewTab, false);
      // Используем синтаксис JQuery как в PNET-frontend для создания похожего модального окна
      $('#autochecker-modal-labs').draggable();
    },
    true
  );

  /**
   * Функция переключения отображения модального окна с iframe'ом
   */
  function autoCheckerModalToggle() {
    const autoCheckerModalElement = document.getElementById('autochecker-modal-labs');
    if (autoCheckerModalElement == null) return;
    const modalIsHidden = autoCheckerModalElement.style.display === 'none';
    autoCheckerModalElement.classList[modalIsHidden ? 'add' : 'remove']('in');
    autoCheckerModalElement.style.display = modalIsHidden ? 'block' : 'none';
  }

  /**
   * Функция открытия окна автоматических тестов в новой вкладке
   */
  function autoCheckerModalNewTab() {
    autoCheckerModalToggle();
    window.open(autoCheckerUrl, '_blank');
  }
});
