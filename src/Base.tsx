import { createBrowserRouter, createRoutesFromElements, Route, RouterProvider } from 'react-router-dom';
import { useQueryPnetInternalAuthApi } from '@/packages/rq/use-query-pnet-internal-auth-api.ts';
import UserAutoChecker from '@/apps/userAutoChecker/userAutoChecker.tsx';

export enum RouterPnetInjectPaths {
  autoChecker = 'user/autoChecker/view',
  // @ts-ignore
  base = import.meta.env.PROD ? 'store/public' : 'pnet'
}

const routerPnetInjectProvider = (
  <Route path={RouterPnetInjectPaths.base.toString()}>
    <Route path={RouterPnetInjectPaths.autoChecker.toString()} element={<UserAutoChecker />} />
  </Route>
);

function RouterProviderBase() {
  const query = useQueryPnetInternalAuthApi();
  if (query.isFetching) return null;
  const router = createBrowserRouter(createRoutesFromElements(<Route path='/'>{routerPnetInjectProvider}</Route>));
  return <RouterProvider router={router} />;
}

export default RouterProviderBase;
