import { nanoid } from 'nanoid';
import {
  WebSocketSetupFn,
  IWebSocketSetup,
  WebSocketAddHandlerProps,
  WebSocketEventMessage,
  ClientWsMessage
} from './client-ws-types.ts';

export class WebSocketPnetGotify {
  private socket?: WebSocket;

  private opened: boolean;

  private setupFn: WebSocketSetupFn;

  private handlers: Map<string, WebSocketAddHandlerProps>;

  constructor(setupFn: WebSocketSetupFn) {
    this.opened = false;
    this.socket = undefined;
    this.setupFn = setupFn;
    this.handlers = new Map();
  }

  addHandler<T>(props: WebSocketAddHandlerProps<T>): string {
    this.open();
    const uuid = nanoid();
    this.handlers.set(uuid, props);
    return uuid;
  }

  removeHandler(id: string): boolean {
    return this.handlers.delete(id);
  }

  private onMessage({ data }: WebSocketEventMessage) {
    const message = JSON.parse(data) as ClientWsMessage;
    this.handlers.forEach((props) => {
      if (
        (props.channel === message.extras.channel || props.channel === undefined) &&
        (props.event === message.extras.event || props.event === undefined)
      ) {
        props.handler(message);
      }
    });
  }

  private open() {
    if (this.opened) return this;
    this.opened = true;
    this.setupFn().then((config) => {
      this.startup(config);
    });
    return this;
  }

  private startup(setup: IWebSocketSetup) {
    this.socket = new WebSocket(setup.url);
    this.socket.onmessage = this.onMessage.bind(this);
  }
}
