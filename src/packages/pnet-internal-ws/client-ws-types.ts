export interface ClientWsMessageExtra<T = any> {
  channel: string;
  event: string;
  payload: T;
  sender: string;
}

export interface ClientWsMessage<T = any> {
  id: number;
  appid: number;
  /* message: Отображение всего сообщения */
  message: string;
  /* title: channel:<название канала> */
  title: string;
  priority: number;
  extras: ClientWsMessageExtra<T>;
  date: string;
}

export interface IWebSocketSetup {
  url: string;
  cluster: string;
}

export type WebSocketSetupFn = () => Promise<IWebSocketSetup>;

export interface WebSocketAddHandlerProps<T = any> {
  event?: string;
  channel?: string;
  handler: (message: ClientWsMessage<T>) => void;
}

export interface WebSocketEventMessage {
  data: string;
}
