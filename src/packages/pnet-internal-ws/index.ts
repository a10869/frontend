import { PnetInternalApi } from '@/packages/pnet-internal-api';
import { WebSocketPnetGotify } from './client-ws.ts';

export const webSocketPnetInternalGotify = new WebSocketPnetGotify(
  PnetInternalApi.webSocket.get.bind(PnetInternalApi.webSocket)
);
