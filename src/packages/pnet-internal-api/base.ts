import { nanoid } from 'nanoid';

export class PnetInternalError extends Error {
  public body: string;

  constructor(body: string) {
    super();
    this.body = body;
  }
}

export interface IPnetInternalResponse<R> {
  result: boolean;
  message?: unknown;
  data: R;
}

export class PnetInternalBaseRequestClass {
  constructor(private readonly url: string) {
    this.url = url.endsWith('/') ? url.slice(0, -1) : url;
  }

  async Call<S = unknown, R = unknown>(
    method: string,
    path?: string,
    queryParams?: URLSearchParams,
    requestData?: R,
    headers?: HeadersInit
  ): Promise<IPnetInternalResponse<S>> {
    const url = this.url + (path ? `/${path}` : '') + (queryParams ? `?${queryParams.toString()}` : '');
    const result = await fetch(url, {
      method,
      body: JSON.stringify(requestData),
      headers: {
        'Content-Type': 'application/json',
        'x-request-id': nanoid(),
        ...headers
      },
      credentials: 'include'
    });

    if (result.status === 500) {
      const responseJson = await result.json();
      throw new PnetInternalError(responseJson?.result || 'Internal error');
    }

    if (result.status >= 200 && result.status < 300) return result.json();
    throw new Error('network error');
  }

  async makeCall<S = unknown, R = unknown>(
    method: string,
    path?: string,
    queryParams?: URLSearchParams,
    requestData?: R,
    headers?: HeadersInit
  ): Promise<S> {
    const response = await this.Call<S, R>(method, path, queryParams, requestData, headers);
    return response.data;
  }
}

export default PnetInternalBaseRequestClass;
