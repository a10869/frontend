export type IJobStatusAutoChecker = 'success' | 'warning' | 'error' | 'wait' | 'run' | 'cancel';

export interface IJobAutoChecker {
  id: number;
  step_id: number;
  status: IJobStatusAutoChecker;
  created_at: string;
  updated_at: string;
  logs_path?: string;
  completed_at?: string;
  user_id?: number;
}

export interface IPipelineAutoChecker {
  id: number;
  title?: string;
  description?: string;
  lab_id: number;
}

export interface IUserAutoCheckerGetResponse {
  is_admin: boolean;
  pipelines: IPipelineAutoChecker[];
  jobs: IJobAutoChecker[];
}

export interface ILogsAutoChecker {
  content: string;
  next: number;
}

export interface IUserAutoCheckerLogsResponse {
  job?: IJobAutoChecker;
  logs?: ILogsAutoChecker;
}
