export interface IWebSocketPnetLabApiGetResponse {
  url: string;
  cluster: string;
}
