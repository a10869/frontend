import { type IUserAutoCheckerGetResponse, type IUserAutoCheckerLogsResponse } from './user-auto-checker-types.ts';
import { PnetInternalBaseRequestClass } from './base.ts';

export class UserAutoCheckerApi extends PnetInternalBaseRequestClass {
  constructor() {
    super('/store/public/user/autoChecker');
  }

  async get(): Promise<IUserAutoCheckerGetResponse> {
    return this.makeCall<IUserAutoCheckerGetResponse>('POST', 'get');
  }

  async run(id: string): Promise<null> {
    return this.makeCall<null>('POST', 'run', new URLSearchParams({ id }));
  }

  async wipe(): Promise<null> {
    return this.makeCall<null>('POST', 'wipe');
  }

  async cancel(id: string): Promise<null> {
    return this.makeCall<null>('POST', 'cancel', new URLSearchParams({ id }));
  }

  async logs(id: number, next: number): Promise<IUserAutoCheckerLogsResponse> {
    return this.makeCall<IUserAutoCheckerLogsResponse>(
      'POST',
      'logs',
      new URLSearchParams({
        id: id.toString(),
        offset: next.toString()
      })
    );
  }
}
