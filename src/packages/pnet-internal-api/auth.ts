import { PnetInternalBaseRequestClass } from './base.ts';
import { type IAuthLoginParams } from '@/packages/pnet-internal-api/auth-types.ts';

export class AuthApi extends PnetInternalBaseRequestClass {
  constructor() {
    super('/store/public/auth/login');
  }

  async auth(username: string, password: string): Promise<unknown> {
    return this.Call<unknown, IAuthLoginParams>('POST', 'login', undefined, {
      username,
      password,
      captcha: '',
      html: '1'
    });
  }
}
