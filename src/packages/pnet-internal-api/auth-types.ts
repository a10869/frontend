export interface IAuthLoginParams {
  username: string;
  password: string;
  captcha: string;
  html: string;
}
