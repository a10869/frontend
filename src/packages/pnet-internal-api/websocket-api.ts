import { PnetInternalBaseRequestClass } from './base.ts';
import { IWebSocketPnetLabApiGetResponse } from './websocket-api-types.ts';

export class WebSocketApi extends PnetInternalBaseRequestClass {
  constructor() {
    super('/store/public/user/webSocket');
  }

  async get(): Promise<IWebSocketPnetLabApiGetResponse> {
    return this.makeCall<IWebSocketPnetLabApiGetResponse>('POST', 'get');
  }
}
