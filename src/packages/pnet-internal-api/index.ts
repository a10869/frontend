import { UserAutoCheckerApi } from './user-auto-checker.ts';
import { AuthApi } from './auth.ts';
import { WebSocketApi } from './websocket-api.ts';

export const PnetInternalApi = {
  userAutoChecker: new UserAutoCheckerApi(),
  webSocket: new WebSocketApi(),
  auth: new AuthApi()
};
