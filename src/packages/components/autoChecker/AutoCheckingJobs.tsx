import { Card, Collapse, Table } from 'react-bootstrap';
import { type IJobAutoChecker } from '@/packages/pnet-internal-api/user-auto-checker-types.ts';
import AutoCheckingBadge from '@/packages/components/autoChecker/AutoCheckingBage.tsx';
import { formatterLaunchedDate } from './utils.ts';
import { DurationTimeLabel } from '@/packages/components/utilsComponents/DurationLabel.tsx';

interface AutoCheckingJobProps {
  iteration: number;
  job: IJobAutoChecker;
  onClick?: (id: number) => void;
}

function AutoCheckingJob({ job, iteration, onClick }: AutoCheckingJobProps) {
  return (
    <tr style={{ cursor: 'pointer' }} onClick={onClick?.bind(onClick, job.id)}>
      <th scope='row'>{iteration}</th>
      <td>
        <AutoCheckingBadge status={job.status} />
      </td>
      <td>
        <DurationTimeLabel createdAt={job.created_at} completedAt={job.completed_at} />
      </td>
      <td>{formatterLaunchedDate(job.created_at)}</td>
    </tr>
  );
}

function AutoCheckingJobHeader() {
  return (
    <tr>
      <th scope='col'>#</th>
      <th scope='col'>Статус</th>
      <th scope='col'>Время</th>
      <th scope='col'>Создана</th>
    </tr>
  );
}

interface AutoCheckingJobsProps {
  isOpen?: boolean;
  onSelectJob?: (id: number) => void;
  jobs: IJobAutoChecker[];
}

function AutoCheckingJobs({ isOpen, onSelectJob, jobs }: AutoCheckingJobsProps) {
  const jobsBlock = jobs.map((job, index) => (
    <AutoCheckingJob key={job.id} iteration={index + 1} job={job} onClick={onSelectJob} />
  ));
  return (
    <Collapse in={isOpen} className='mt-3 mb-3'>
      <Card>
        <Table hover>
          <thead>
            <AutoCheckingJobHeader />
          </thead>
          <tbody>{jobsBlock}</tbody>
        </Table>
      </Card>
    </Collapse>
  );
}

export default AutoCheckingJobs;
