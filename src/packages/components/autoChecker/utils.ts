import dayjs, { Dayjs } from 'dayjs';

export function formatterLaunchedDate(launchedDate: string | Dayjs | null, format?: string): string {
  return dayjs(launchedDate)
    .add(dayjs().utcOffset(), 'minutes')
    .format(format || 'HH:mm DD.MM.YYYY');
}

export const useStateJobsView = new Map<number, boolean>();
