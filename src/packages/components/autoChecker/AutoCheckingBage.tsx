import {
  faCircle,
  faCircleCheck,
  faCircleDot,
  faCircleExclamation,
  faCircleXmark
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { type IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { type IJobStatusAutoChecker } from '@/packages/pnet-internal-api/user-auto-checker-types.ts';

interface AutoCheckingBadgeProps {
  status: IJobStatusAutoChecker;
}

interface TAutoCheckingBadgeStylesItem {
  class: string;
  icon: IconDefinition;
  text: string;
}

function AutoCheckingBadge({ status }: AutoCheckingBadgeProps) {
  const styles: {
    [key in IJobStatusAutoChecker]: TAutoCheckingBadgeStylesItem;
  } = {
    success: {
      class: 'success',
      icon: faCircleCheck,
      text: 'Успешно'
    },
    warning: {
      class: 'warning',
      icon: faCircleExclamation,
      text: 'Ошибка'
    },
    error: {
      class: 'danger',
      icon: faCircleXmark,
      text: 'Ошибка'
    },
    wait: {
      class: 'light',
      icon: faCircle,
      text: 'Ожидает'
    },
    run: {
      class: 'secondary',
      icon: faCircleDot,
      text: 'Запущена'
    },
    cancel: {
      class: 'light',
      icon: faCircleXmark,
      text: 'Отменена'
    }
  };
  const styleStatus = styles[status];
  return (
    <span
      className={`badge align-items-center text-${styleStatus.class}-emphasis bg-${styleStatus.class}-subtle border border-${styleStatus.class}-subtle rounded-pill`}
    >
      <FontAwesomeIcon icon={styleStatus.icon} className='me-1' />
      <span className='autochecking-bage-text'>{styleStatus.text}</span>
    </span>
  );
}

export default AutoCheckingBadge;
