import { Button, Col, Container, Row } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBroom, faRotateRight } from '@fortawesome/free-solid-svg-icons';
import AutoCheckingBlock from '@/packages/components/autoChecker/AutoCheckingBlock.tsx';
import { actionsApiAutoChecker, wipeApiAutoChecker } from '@/apps/userAutoChecker/use-query-get.tsx';
import { TAutoCheckingActions } from '@/packages/components/autoChecker/AutoCheckingActions.tsx';
import { IPipelineAutoCheckerWithJobs } from '@/apps/userAutoChecker/use-query-get-user-pipelines-rt.tsx';

interface AutoCheckingSectionProps {
  broomView: boolean;
  pipelines: IPipelineAutoCheckerWithJobs[];
  refreshFn: () => void;
  jobOnSelect?: (id: number) => void;
}

function AutoCheckingSection({ broomView, pipelines, refreshFn, jobOnSelect }: AutoCheckingSectionProps) {
  const actionsFn = (id: number, action: TAutoCheckingActions) => {
    actionsApiAutoChecker(refreshFn, `${id}`, action);
  };
  const blocks = pipelines.map((item) => (
    <AutoCheckingBlock pipeline={item} key={item.id} actionOnSelect={actionsFn} jobOnSelect={jobOnSelect} />
  ));
  return (
    <Container fluid>
      <Row className='justify-content-end p-2'>
        {broomView ? (
          <Col xs='2' sm='1'>
            <Button
              onClick={() => {
                wipeApiAutoChecker(refreshFn);
              }}
              variant=''
            >
              <FontAwesomeIcon icon={faBroom} />
            </Button>
          </Col>
        ) : null}
        <Col xs='2' sm='1'>
          <Button onClick={refreshFn} variant=''>
            <FontAwesomeIcon icon={faRotateRight} />
          </Button>
        </Col>
      </Row>
      {blocks}
    </Container>
  );
}

export default AutoCheckingSection;
