import { useState } from 'react';
import { Col, Row } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCalendar, faClock } from '@fortawesome/free-solid-svg-icons';
import AutoCheckingActions, {
  type TAutoCheckingActions
} from '@/packages/components/autoChecker/AutoCheckingActions.tsx';
import AutoCheckingBadge from '@/packages/components/autoChecker/AutoCheckingBage.tsx';
import AutoCheckingJobs from '@/packages/components/autoChecker/AutoCheckingJobs.tsx';
import { formatterLaunchedDate, useStateJobsView } from './utils.ts';
import { DurationTimeLabel } from '@/packages/components/utilsComponents/DurationLabel.tsx';
import { IPipelineAutoCheckerWithJobs } from '@/apps/userAutoChecker/use-query-get-user-pipelines-rt.tsx';

interface AutoCheckingBlockProps {
  pipeline: IPipelineAutoCheckerWithJobs;
  actionOnSelect?: (id: number, action: TAutoCheckingActions) => void;
  jobOnSelect?: (id: number) => void;
}

function AutoCheckingBlock({ pipeline, actionOnSelect, jobOnSelect }: AutoCheckingBlockProps) {
  const [jobsView, setJobsView] = useState<boolean>(useStateJobsView.get(pipeline.id) || false);
  const collapseJobsFn = () => {
    if (!pipeline.jobs) return;
    useStateJobsView.set(pipeline.id, !jobsView);
    setJobsView(!jobsView);
  };
  const lastStatus = {
    status: pipeline?.jobs?.[0]?.status || 'wait',
    created_at: pipeline?.jobs?.[0]?.created_at,
    completed_at: pipeline?.jobs?.[0]?.completed_at
  };
  return (
    <Row className='p-2'>
      <Col xs='3'>
        <div className='mb-2'>
          <AutoCheckingBadge status={lastStatus.status} />
        </div>
        {lastStatus.created_at ? (
          <p style={{ fontSize: '0.725rem' }} className='ms-1 m-0'>
            <FontAwesomeIcon icon={faClock} className='me-1' />
            <DurationTimeLabel createdAt={lastStatus.created_at} completedAt={lastStatus.completed_at} />
          </p>
        ) : null}
        {lastStatus.created_at ? (
          <p style={{ fontSize: '0.725rem' }} className='ms-1 m-0'>
            <FontAwesomeIcon icon={faCalendar} className='me-1' />
            {formatterLaunchedDate(lastStatus.created_at)}
          </p>
        ) : null}
      </Col>
      <Col xs='8'>
        <p className='text-start fw-bold m-0'>{pipeline.title}</p>
        <p className='text-start'>{pipeline.description}</p>
      </Col>
      <Col xs='1'>
        <AutoCheckingActions
          id={pipeline.id}
          status={lastStatus.status}
          onSelect={actionOnSelect}
          collapseJobsFn={pipeline.jobs.length > 0 ? collapseJobsFn : undefined}
        />
      </Col>
      <AutoCheckingJobs isOpen={jobsView} jobs={pipeline.jobs} onSelectJob={jobOnSelect} />
    </Row>
  );
}

export default AutoCheckingBlock;
