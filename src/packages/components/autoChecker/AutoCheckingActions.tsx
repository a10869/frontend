import { Dropdown } from 'react-bootstrap';
import { IJobStatusAutoChecker } from '@/packages/pnet-internal-api/user-auto-checker-types';

export type TAutoCheckingActions = 'restart' | 'start' | 'cancel';

interface AutoCheckingActionsProps {
  id: number;
  status: IJobStatusAutoChecker;
  onSelect?: (id: number, action: TAutoCheckingActions) => void;
  collapseJobsFn?: () => void;
}

function AutoCheckingActions({ id, status, onSelect, collapseJobsFn }: AutoCheckingActionsProps) {
  const mappingActions: {
    [key in IJobStatusAutoChecker]: TAutoCheckingActions[];
  } = {
    success: ['restart'],
    warning: ['restart'],
    error: ['restart'],
    wait: ['start'],
    run: ['cancel'],
    cancel: ['restart']
  };
  const actionToText: { [key in TAutoCheckingActions]: string } = {
    restart: 'Перезапустить',
    start: 'Запустить',
    cancel: 'Отменить'
  };
  const dropDownBlocks = mappingActions[status].map((item, index) => (
    <Dropdown.Item
      key={`drop-down-action-${index}`}
      onClick={() => {
        onSelect?.(id, item);
      }}
    >
      {actionToText[item]}
    </Dropdown.Item>
  ));
  return (
    <Dropdown align='end' drop='down'>
      <Dropdown.Toggle size='sm' variant='' split={false} />
      <Dropdown.Menu>
        {collapseJobsFn && <Dropdown.Item onClick={collapseJobsFn}>Показать попытки</Dropdown.Item>}
        {dropDownBlocks}
      </Dropdown.Menu>
    </Dropdown>
  );
}

export default AutoCheckingActions;
