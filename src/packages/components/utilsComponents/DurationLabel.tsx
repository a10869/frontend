import { useCallback, useEffect, useState } from 'react';
import dayjs, { Dayjs } from 'dayjs';

interface DurationTimeLabelProps {
  createdAt: string;
  completedAt?: string;
}

export function DurationTimeLabel({ createdAt, completedAt }: DurationTimeLabelProps) {
  const [currentDate, setCurrentDate] = useState<Dayjs | null>(null);
  const diffFn = useCallback(() => {
    return dayjs(dayjs().subtract(dayjs().utcOffset(), 'minutes').diff(dayjs(createdAt)));
  }, [createdAt]);

  useEffect(() => {
    if (createdAt && completedAt) {
      const diff = dayjs(dayjs(completedAt).diff(dayjs(createdAt)));
      setCurrentDate(diff);
      return;
    }
    setCurrentDate(diffFn());
    const timerRef = setInterval(() => {
      setCurrentDate(diffFn());
    }, 1_000);

    // eslint-disable-next-line consistent-return
    return () => {
      clearInterval(timerRef);
    };
  }, [diffFn, createdAt, completedAt]);
  return <span>{currentDate?.subtract(dayjs().utcOffset(), 'minutes')?.format('HH:mm:ss') || ''}</span>;
}
