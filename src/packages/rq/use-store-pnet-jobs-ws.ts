import { useEffect, useState } from 'react';
import { IJobAutoChecker } from '@/packages/pnet-internal-api/user-auto-checker-types.ts';
import { ClientWsMessage } from '@/packages/pnet-internal-ws/client-ws-types.ts';
import { webSocketPnetInternalGotify } from '@/packages/pnet-internal-ws';
import { MergeUtilsAbstract } from './mergeUtils.ts';

type Message = ClientWsMessage<IJobAutoChecker>;

class AutoCheckerJobsUpdatePipeline extends MergeUtilsAbstract<Message> {
  // eslint-disable-next-line class-methods-use-this
  public GrantEqual(first: Message, second?: Message): boolean {
    if (!second) return true;
    return first.extras.payload.updated_at >= second.extras.payload.updated_at;
  }

  // eslint-disable-next-line class-methods-use-this
  public Index(first: Message): string {
    return `${first.extras.event}-${first.extras.sender}-${first.extras.payload.id}`;
  }
}

export function useStoreAutoCheckerJobsUpdatedWs(): Message[] {
  const [store, setStore] = useState<AutoCheckerJobsUpdatePipeline>(new AutoCheckerJobsUpdatePipeline());
  useEffect(() => {
    const handlerRef = webSocketPnetInternalGotify.addHandler<IJobAutoChecker>({
      channel: 'JobsUpdate',
      event: 'AutoCheckerJobsUpdated',
      handler: (message) => {
        if (!store.addItem(message)) return;
        setStore(new AutoCheckerJobsUpdatePipeline(store.getStore()));
      }
    });
    return () => {
      webSocketPnetInternalGotify.removeHandler(handlerRef);
    };
  }, []);
  return store.toArray();
}
