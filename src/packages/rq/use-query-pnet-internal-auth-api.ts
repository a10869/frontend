import { useQuery } from '@tanstack/react-query';
import { PnetInternalApi } from '@/packages/pnet-internal-api';

export function useQueryPnetInternalAuthApi() {
  return useQuery(['useQueryPnetInternalAuthApi'], async () => {
    if (import.meta.env.PROD) return true;
    await PnetInternalApi.auth.auth('', '');
    await PnetInternalApi.auth.auth('admin', 'pnet');
    return true;
  });
}
