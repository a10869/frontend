interface IMergeUtils<T> {
  GrantEqual(first: T, second?: T): boolean;

  Index(first: T): string;

  update(other: IMergeUtils<T>): void;

  addItem(item: T): boolean;

  fromArray(first: T[]): void;

  getStore(): Map<string, T>;

  toArray(): T[];
}

export abstract class MergeUtilsAbstract<T> implements IMergeUtils<T> {
  private store: Map<string, T>;

  constructor(store: Map<string, T> | undefined = undefined) {
    this.store = store ?? new Map();
  }

  public fromArray(first: T[]): void {
    first.forEach(this.addItem.bind(this));
  }

  public getStore(): Map<string, T> {
    return this.store;
  }

  public toArray(): T[] {
    return Array.from(this.store.values());
  }

  public update(other: IMergeUtils<T>): void {
    other.getStore().forEach((item) => {
      const index = other.Index(item);
      const selfItem = this.store.get(index);
      if (selfItem === undefined) return;
      if (!this.GrantEqual(item, selfItem)) return;
      this.store.set(index, item);
    });
  }

  public addItem(item: T): boolean {
    const index = this.Index(item);
    if (this.GrantEqual(item, this.store.get(index))) {
      this.store.set(index, item);
      return true;
    }
    return false;
  }

  abstract GrantEqual(first: T, second?: T): boolean;

  abstract Index(first: T): string;
}
