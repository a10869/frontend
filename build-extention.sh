#!/bin/bash
set -e
rm -rf ./dist
yarn build
cd ./dist
FROM="/assets"
TO="/store/public/main/js/pnetlabapijs$FROM"
echo "Replacing paths from:$FROM to:$TO"

if [[ "$OSTYPE" == "darwin"* ]]; then
  # macOS
  sed -i '' -e "s#$FROM#$TO#g" ./index.html
else
  # Linux
  sed -i -e "s#$FROM#$TO#g" ./index.html
fi
mv ./index.html ./main.blade.php
mkdir -p ./pnetlab-inject/pnetlabapijs
mv ./* ./pnetlab-inject || echo "removed"
mv ./pnetlab-inject/assets ./pnetlab-inject/pnetlabapijs
cp -r ../src/pnetlab-inject/* ./pnetlab-inject
zip -r pnetlab-inject.zip pnetlab-inject
rm -rf ./pnetlab-inject
cd ..
echo "Build inject extension successfully"
